clear
close all
addpath LIM/scripts/

load GLDAS.txt

colNames={'Tair_f_inst'
'Qair_f_inst'
'Rainf_f_tavg'
'SoilMoi0_10cm_inst'
'SoilMoi10_40cm_inst'
'SoilMoi40_100cm_inst'
'Evap_tavg'
'LWdown_f_tavg'
'Lwnet_tavg'
'Qg_tavg'
'Qh_tavg'
'Qle_tavg'
'SWdown_f_tavg'
'Swnet_tavg'
'PotEvap_tavg'
'Tveg_tavg'};

dayStartStr='5/15/12 12:00';


[nt,nv]=size(GLDAS);
ndays=nt/8;
datenums=(datenum(dayStartStr):datenum(dayStartStr)+ndays-1);

GLDAS_daily=nan(ndays,nv);
k=1;
tstart=2012+(datenum('5/15/12 12:00')-datenum('1/1/12 12:00'))/365;

for i =1:ndays
    q=k:k+7;
    GLDAS_daily(i,:)=mean(GLDAS(q,:));
    if i==1
        time(i)=tstart;
    else
        time(i)=time(i-1)+1/365;
    end
    k=k+8;   
end

%% Filter out annual cycle
per=30;
GLDAS_daily_filt=nan(size(GLDAS_daily));
for i =1:nv
    y=butterfilt(GLDAS_daily(:,i),'high',3,per,nan);
    GLDAS_daily_filt(:,i)=y;
end
%%
use_these={'Qair_f_inst','SoilMoi0_10cm_inst'};
colq=[];
for i =1:length(use_these);
    colq=[colq; find(strcmp(use_these{i},colNames))];
end
%%
t=1;
for time_i=1:nt-31
        tq=t:t+30;
    X=rm_mtx_mean(GLDAS_daily_filt(tq,colq));
    %% Test legitimacy of LIM
    % define lag for the covariance matrix (Ctau)
    tau0=7;

    Ctau0=lagCov(X,X,tau0);
    Co=cov(X);

    % Calculate L (PS95, eq. [4])
    Btau0=(1./tau0)*logm(Ctau0*pinv(Co));%

    Btau0_eigs=real(eigs(Btau0));
    if any(Btau0_eigs>0)
         error('Failed: Eigenvalues of B have real positive parts')      
    end

    disp('Passed: Eigenvalues of B do not have real positive parts')
    %% Is [Q] (determined from [B][C0]+[C0][B]''+[Q]=0) positive definite?
    disp('3) Is [Q] (determined from [B][C0]+[C0][B]''+[Q]=0) positive definite?')
    L=Btau0;

    tau=tau0;
    Ctau=Ctau0;
    B_eigs=Btau0_eigs;
    Q=-(L*Co+Co*L');
    [Qeofs,Qv]=svd(Q);
    if(any(diag(Qv)<0)) 
        error('Some eigenvalues of Q <0. These should be eliminated.');    
    end
    Qeigs=diag(Qv);
    %% Normalizing Qeofs

    clear Qscld
    nQeigs=length(Qeigs);
    for i =1:nQeigs
        Qscld(:,i)=Qeofs(:,i).*sqrt(Qv(i,i));
    end 
    %% Decoupled part
    L_dcpld=L;

    L_dcpld(1,2)=0;
    L_dcpld(2,1)=0;
    %% Stochastic forcing part
    nt=100;

    timeUnit='Day';
    nHrsInDay=24;
    nMinutesInHour=60;
    nMinutesInRn=60;
    deltaT=nMinutesInRn/(nMinutesInHour*nHrsInDay);  %6hrly data = 6/(24*30)=1/120

    Xinit=X(1,:)';
    dayLen=24;
    nDayExtra=1;
    nDay=nt;

    nr=100;
    Omega_cr=nan(nr,1);
    Omega_cv=nan(nr,1);
    clear cc cd
    for i =1:nr;

        Xrand=stoForceFn(Xinit,L,Qscld,nQeigs,nDay,deltaT,dayLen,nDayExtra);
        XrandDecoup=stoForceFn(Xinit,L_dcpld,Qscld,nQeigs,nDay,deltaT,dayLen,nDayExtra);

        ccmtx=cov(Xrand');
        cdmtx=cov(XrandDecoup');
        cc=ccmtx(1,2);
        cd=cdmtx(1,2);
%         cci(i)=cc;
%         cdi(i)=cd;

        Omega_cv(i)=(cc-cd)./cc;
        Omega_cr(i)=(corr(Xrand(1,:)',Xrand(2,:)')-corr(XrandDecoup(1,:)',XrandDecoup(2,:)'))/corr(Xrand(1,:)',Xrand(2,:)');

%         crci(i)=corr(Xrand(1,:)',Xrand(2,:)');
%         crdi(i)=corr(XrandDecoup(1,:)',XrandDecoup(2,:)');
    end
    Omegat_cv(t)=mean(Omega_cv);
    Omegat_cr(t)=mean(Omega_cr);
    
    t=t+1;
end
    