clear
close all

%% Build the pseudo-data
x=nan(2001,1);
y=nan(2001,1);

y(1)=rand;
x(1)=randn;
a1=0.6;
b1=0.1;
c1=1;

a2=0.9;
b2=0.04;
c2=1;

for i =2:2001
    x(i)=x(i-1)*a1+y(i-1)*b1+randn*c1;
    
    y(i)=y(i-1)*a2+x(i-1)*b2+randn*c2;
    
end

%%
X=rm_mtx_mean([x(end-999:end) y(end-999:end)]);
%% Test legitimacy of LIM
% define lag for the covariance matrix (Ctau)
tau0=7;

Ctau0=lagCov(X,X,tau0);
Co=cov(X);

% Calculate L (PS95, eq. [4])
Btau0=(1./tau0)*logm(Ctau0*pinv(Co));%

Btau0_eigs=real(eigs(Btau0));
if any(Btau0_eigs>0)
     error('Failed: Eigenvalues of B have real positive parts')      
end

disp('Passed: Eigenvalues of B do not have real positive parts')
%% Is [Q] (determined from [B][C0]+[C0][B]''+[Q]=0) positive definite?
disp('3) Is [Q] (determined from [B][C0]+[C0][B]''+[Q]=0) positive definite?')
L=Btau0;

tau=tau0;
Ctau=Ctau0;
B_eigs=Btau0_eigs;
Q=-(L*Co+Co*L');
[Qeofs,Qv]=svd(Q);
if(any(diag(Qv)<0)) 
    error('Some eigenvalues of Q <0. These should be eliminated.');    
end
Qeigs=diag(Qv);
%% Normalizing Qeofs

clear Qscld
nQeigs=length(Qeigs);
for i =1:nQeigs
    Qscld(:,i)=Qeofs(:,i).*sqrt(Qv(i,i));
end 
%% Decoupled part
L_dcpld=L;

L_dcpld(1,2)=0;
L_dcpld(2,1)=0;
%% Stochastic forcing part
nt=1000;

timeUnit='Day';
nHrsInDay=24;
nMinutesInHour=60;
nMinutesInRn=60;
deltaT=nMinutesInRn/(nMinutesInHour*nHrsInDay);  %6hrly data = 6/(24*30)=1/120

Xinit=X(1,:)';
dayLen=24;
nDayExtra=1;
nDay=nt;

nr=100;
Omega_cr=nan(nr,1);
Omega_cv=nan(nr,1);
clear cc cd
for i =1:nr;
    
    Xrand=stoForceFn(Xinit,L,Qscld,nQeigs,nDay,deltaT,dayLen,nDayExtra);
    XrandDecoup=stoForceFn(Xinit,L_dcpld,Qscld,nQeigs,nDay,deltaT,dayLen,nDayExtra);
    
    ccmtx=cov(Xrand');
    cdmtx=cov(XrandDecoup');
    cc=ccmtx(1,2);
    cd=cdmtx(1,2);
    cci(i)=cc;
    cdi(i)=cd;
    
    Omega_cv(i)=(cc-cd)./cc;
    Omega_cr(i)=(corr(Xrand(1,:)',Xrand(2,:)')-corr(XrandDecoup(1,:)',XrandDecoup(2,:)'))/corr(Xrand(1,:)',Xrand(2,:)');
    
    crci(i)=corr(Xrand(1,:)',Xrand(2,:)');
    crdi(i)=corr(XrandDecoup(1,:)',XrandDecoup(2,:)');
end
